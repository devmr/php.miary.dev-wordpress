<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>

<section class="project-wrapper single-col-max-width py-5 px-4 mx-auto">
    <div class="section-row">
	<?php

    $archive_title    = get_the_archive_title();
    $archive_subtitle = get_the_archive_description();

    if ( $archive_title || $archive_subtitle ) {
        ?>

        <header class="archive-header has-text-align-center header-footer-group">

            <div class="archive-header-inner section-inner medium">

                <?php if ( $archive_title ) { ?>
                    <h1 class="archive-title"><?php echo wp_kses_post( $archive_title ); ?></h1>
                <?php } ?>

                <?php if ( $archive_subtitle ) { ?>
                    <div class="archive-subtitle section-inner thin max-percentage intro-text"><?php echo wp_kses_post( wpautop( $archive_subtitle ) ); ?></div>
                <?php } ?>

            </div><!-- .archive-header-inner -->

        </header><!-- .archive-header -->

        <?php
    }

	if ( have_posts() ) {

		while ( have_posts() ) {
			the_post();
?>
        <div class="media mb-5 flex-column flex-lg-row bg-white shadow-sm">
            <div class="media-body p-4">
                <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                <ul class="talk-meta list-inline mb-2">
                    <li class="list-inline-item mr-3"><i class="far fa-clock mr-2"></i><?php the_date(); ?></li>
                    <li class="list-inline-item"><?php the_category(); ?></li>
                </ul>
                <?php
			        get_template_part( 'template-parts/content', get_post_type() );
                ?>
            </div></div>
        <?php
		}
	}

	?>

</div><!-- #site-content -->
</section><!-- #site-content -->

<?php get_footer(); ?>
