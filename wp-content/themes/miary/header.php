<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >

		<link rel="profile" href="https://gmpg.org/xfn/11">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
        <link href='//fonts.googleapis.com/css?family=Roboto:400,500,400italic,300italic,300,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />

		<?php wp_head(); ?>

	</head>

	<body <?php body_class(); ?>>

		<?php
		wp_body_open();
		?>

        <header class="header">

            <div class="top-bar theme-bg-primary-darken">

                <div class="container-fluid">

                    <nav class="navbar navbar-expand-lg navbar-dark position-relative">


                        <ul class="social-list list-inline mb-0">


                            <?php
                            $social_network_gitlab = get_option('social_network_gitlab');
                            if ('' !== $social_network_gitlab) :
                                ?>
                                <li class="list-inline-item"><a class="text-white" href="<?= $social_network_gitlab ; ?>" title="Gitlab" target="_blank" rel="noopener"><i class="fab fa-gitlab fa-fw"></i></a></li>
                            <?php endif; ?>


                            <?php
                            $social_network_github = get_option('social_network_github');
                            if ('' !== $social_network_github) :
                                ?>
                                <li class="list-inline-item"><a class="text-white" href="<?= $social_network_github ; ?>" title="Github" target="_blank" rel="noopener"><i class="fab fa-github-alt fa-fw"></i></a></li>
                            <?php endif; ?>

                            <?php
                            $social_network_linkedin = get_option('social_network_linkedin');
                            if ('' !== $social_network_linkedin) :
                                ?>
                                <li class="list-inline-item"><a class="text-white" href="<?= $social_network_linkedin; ?>" title="Linkedin" target="_blank" rel="noopener"><i class="fab fa-linkedin-in fa-fw"></i></a></li>
                            <?php endif; ?>

                            <?php
                            $social_network_twitter = get_option('social_network_twitter');
                            if ('' !== $social_network_twitter) :
                                ?>
                                <li class="list-inline-item"><a class="text-white" href="<?= $social_network_twitter; ?>" title="twitter" target="_blank" rel="noopener"><i class="fab fa-twitter fa-fw"></i></a></li>
                            <?php endif; ?>

                            <?php
                                $social_network_skype = get_option('social_network_skype');
                                if ('' !== $social_network_skype) :
                            ?>
                            <li class="list-inline-item"><a class="text-white" href="callto:<?= $social_network_skype; ?>" title="Skype" target="_blank" rel="noopener"><i class="fab fa-skype fa-fw"></i></a></li>
                            <?php endif; ?>





                        </ul><!--//social-list-->


                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                            <?php
                            wp_nav_menu( array(
                                'theme_location'  => 'primary',
                                'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
                                'container'       => 'div',
                                'container_class' => 'collapse navbar-collapse text-uppercase',
                                'container_id'    => 'navigation',
                                'menu_class'      => 'navbar-nav ml-lg-auto',
                                'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                                'walker'          => new WP_Bootstrap_Navwalker(),
                            ) );
                            ?>
                            <span id="slide-line"></span>




                    </nav>

                </div><!--//container-->

            </div><!--//top-bar-->


            <?php

                if ('' === locate_template(['template-parts/page-header-'.get_the_ID().'.php'])) {
                ?>

                <!-- Default Header -->
<div class="header-intro theme-bg-primary text-white py-5">
    <div class="container">

        <h2 class="page-heading mb-0"><?php echo get_the_title(); ?></h2>

    </div>
</div>

<?php
                } else {
                    get_template_part('template-parts/page-header-'.get_the_ID());
                }
             ?>


        </header><!--//header-->
		<?php
