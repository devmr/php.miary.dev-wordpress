<?php
/**
 * Template Name: Contact
 */

get_header();
?>
<section class="section py-5">
    <div class="container">
        <div class="row">
            <div class="contact-intro col-lg-12 mx-lg-auto mb-5 text-center">


                <div class="speech-bubble bg-white p-4 p-lg-5 shadow-sm">
                     <?php the_content(); ?>
                </div>

            </div><!--//contact-intro-->


        </div><!--//row-->
    </div><!--//container-->
</section><!--//section-->
<?php
get_footer();
