<?php
/**
 * Template Name: Blog
 */

get_header();

// Page query
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

?>
    <section class="section pt-5">
        <div class="container blog-cards">

            <?php
                if (is_active_sidebar( 'sidebar-1' )) {
                    dynamic_sidebar( 'sidebar-1' );
                }
            ?>
            <div class="row">
                <?php

                if (1 === $paged) {
                    $args = array(
                        'post_status' => 'publish',
                        'posts_per_page' => 1,
                        'orderby' => 'date',
                        'order' => 'DESC',
                    );

                    $loop = new WP_Query( $args );

                    while ( $loop->have_posts() ){
                        $loop->the_post();
                        ?>
                        <div class="col-12">
                            <div class="featured-card d-md-table card rounded-0 border-0 shadow-sm mb-5">
                                <div class="featured-card-image card-img-container position-relative d-md-table-cell">
                                    <div class="card-img-overlay overlay-mask text-center p-0">
                                        <?php
                                        $custom_logo_id = get_post_thumbnail_id();
                                        $image_src = wp_get_attachment_url( $custom_logo_id);
                                        ?>
                                        <img src="<?= $image_src; ?>" alt="">
                                    </div>
                                </div>

                                <div class="featured-card-body card-body d-md-table-cell  pb-4">
                                    <h4 class="card-title mb-2"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                    <div class="card-text">
                                        <div class="excerpt mb-3"><?php the_excerpt(); ?></div>
                                    </div>
                                    <div class="card-footer border-0 d-md-none">
                                        <ul class="meta list-inline mb-0">
                                            <li class="list-inline-item mr-3"><i class="far fa-clock mr-2"></i><?php the_date(); ?></li>
                                            <li class="list-inline-item"><a href="<?php the_field('url'); ?>"><?php the_category(); ?></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    wp_reset_postdata();
                }
                ?>

                <?php

                $args = array(
                    'post_status' => 'publish',
                    'posts_per_page' => 10,
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'paged' => $paged,
                );

                $blogs_query = new WP_Query( $args );

                $i = 0;
                while ( $blogs_query->have_posts() ){
                    $blogs_query->the_post();

                    if ($i>0) :
                    ?>
                    <div class="col-12 col-md-6 col-lg-4 mb-5">
                        <div class="card rounded-0 border-0 shadow-sm eq-height">
                            <div class="card-img-container position-relative">
                                <?php
                                $custom_logo_id = get_post_thumbnail_id();
                                $image_src = wp_get_attachment_url( $custom_logo_id);
                                ?>
                                <img class="card-img-top rounded-0" src="<?= $image_src; ?>" alt="">
                                <div class="card-img-overlay overlay-mask text-center p-0">
                                    <div class="overlay-mask-content text-center w-100 position-absolute">

                                    </div>
                                </div>
                            </div>
                            <div class="card-body pb-4">

                                <h4 class="card-title mb-2"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                <div class="card-text">

                                    <div class="excerpt"><?php the_excerpt(); ?></div>
                                </div>

                            </div>
                            <div class="card-footer border-0">
                                <ul class="meta list-inline mb-0">
                                    <li class="list-inline-item mr-3"><i class="far fa-clock mr-2"></i><?php the_date(); ?></li>
                                    <li class="list-inline-item"><a href="<?php the_field('url'); ?>"><?php the_category(); ?></a></li>
                                </ul>
                            </div>
                        </div><!--//card-->
                    </div>
                    <?php
                    endif;
                    ++$i;
                }
                wp_reset_postdata();
                ?>
            </div>

             <div class="clearfix"><?php echo bootstrap_pagination($blogs_query); ?></div>
        </div>
    </section>
<?php
get_footer();
