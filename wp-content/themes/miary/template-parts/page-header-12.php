<!-- Resume -->
<div class="header-intro header-resume theme-bg-primary text-white py-5">
    <div class="container position-relative">

            <h2 class="page-heading mb-0"><?php echo get_the_title(); ?></h2>
            <a class="btn theme-btn-on-bg download-resume position-absolute font-weight-bold mx-auto"
                href="<?= get_field('cv_fichier_pdf'); ?>"
                rel="noopener" target="_blank"><i class="fas fa-download mr-2"></i><?= get_field('label_bouton_telecharger_cv'); ?></a>


    </div>
</div>

<li class="resume-wrapper text-center position-relative">
    <div class="resume-wrapper-inner mx-auto text-left bg-white shadow-lg">

        <header class="resume-header pt-4 pt-md-0">
            <div class="media flex-column flex-md-row">

                <div class="media-body pt-5 pb-4 px-4 d-flex flex-column flex-md-row mx-auto mx-lg-0">
                    <div class="primary-info">
                        <h1 class="name mt-0 mb-1 text-white text-uppercase text-uppercase"><?php echo esc_html(get_bloginfo( 'blogname' )); ?></h1>
                        <div class="title mb-3"><?php echo esc_html(get_bloginfo( 'description' )); ?></div>
                        <ul class="list-unstyled">
                            <li class="mb-2"><a href="#"><i class="far fa-envelope fa-fw mr-2" data-fa-transform="grow-3"></i><?php echo esc_html(get_bloginfo( 'admin_email' )); ?></a></li>
                            <li><a href="#"><i class="fas fa-mobile-alt fa-fw mr-2" data-fa-transform="grow-6"></i><?php echo esc_html(get_option( 'tel' )); ?></a></li>
                        </ul>
                    </div><!--//primary-info-->
                    <div class="secondary-info ml-md-auto mt-2">
                        <ul class="resume-social list-unstyled">
                            <?php
                            $social_network_gitlab = get_option('social_network_gitlab');
                            if ('' !== $social_network_gitlab) :
                                ?>
                                <li class="mb-3"><a href="<?= $social_network_gitlab; ?>"><span class="fa-container text-center mr-2"><i class="fab fa-gitlab fa-fw"></i></span><?= $social_network_gitlab; ?></a></li>
                            <?php endif; ?>

                            <?php
                            $social_network_github = get_option('social_network_github');
                            if ('' !== $social_network_github) :
                                ?>
                                <li class="mb-3"><a href="<?= $social_network_github; ?>"><span class="fa-container text-center mr-2"><i class="fab fa-github-alt fa-fw"></i></span><?= $social_network_github; ?></a></li>
                            <?php endif; ?>

                            <?php
                            $social_network_twitter = get_option('social_network_twitter');
                            if ('' !== $social_network_twitter) :
                                ?>
                                <li class="mb-3"><a href="<?= $social_network_twitter; ?>"><span class="fa-container text-center mr-2"><i class="fab fa-twitter"></i></span><?= $social_network_twitter; ?></a></li>
                            <?php endif; ?>

                            <?php
                            $social_network_linkedin = get_option('social_network_linkedin');
                            if ('' !== $social_network_linkedin) :
                            ?>
                            <li class="mb-3"><a href="<?= $social_network_linkedin; ?>"><span class="fa-container text-center mr-2"><i class="fab fa-linkedin-in fa-fw"></i></span><?= $social_network_linkedin; ?></a></li>
                            <?php endif; ?>




                        </ul>
                    </div><!--//secondary-info-->

                </div><!--//media-body-->
                <?php
                    $custom_logo_id = get_theme_mod( 'custom_logo' );
                    $image_src = wp_get_attachment_url( $custom_logo_id );
                ?>
                <img width="240" height="240" alt="" src="<?= $image_src; ?>" class="profile-image mb-3 mb-md-0 ml-md-5 mx-auto">
            </div><!--//media-->
        </header>

        <div class="resume-body p-5">
            <section class="resume-section summary-section mb-5">
                <h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">
                    <?= get_field('presentation_titre'); ?>
                </h2>
                <div class="resume-section-content">
                    <p class="mb-0">
                        <?= get_field('presentation'); ?>
                    </p>
                </div>
            </section>

            <div class="row">
                <div class="col-lg-7">

                    <!-- Experience -->
                    <section class="resume-section experience-section mb-5">
                        <h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3"><?= get_field('experience_titre'); ?></h2>
                        <div class="resume-section-content">
                            <div class="resume-timeline position-relative">
                                <?php
                                $args = array(
                                    'post_type' => 'experience',
                                    'post_status' => 'publish',
                                    'posts_per_page' => -1,
                                    'orderby' => 'date',
                                    'order' => 'DESC',
                                    'cat' => 'home',
                                );

                                $loop = new WP_Query( $args );

                                while ( $loop->have_posts() ){
                                    $loop->the_post();
                                    ?>
                                    <article class="resume-timeline-item position-relative pb-5">
                                        <div class="resume-timeline-item-header mb-2">
                                            <div class="d-flex flex-column flex-md-row">
                                                <h3 class="resume-position-title font-weight-bold mb-1"><?= the_title(); ?></h3>
                                                <div class="resume-company-name ml-auto"><?php the_field('societe'); ?></div>
                                            </div><!--//row-->
                                            <div class="resume-position-time"><?= get_field('periode'); ?></div>
                                        </div>

                                        <div class="resume-timeline-item-desc">
                                            <?php
                                                the_content();
                                                ?>
                                        </div>
                                    </article>
                                    <?php
                                }
                                wp_reset_postdata();
                                ?>
                            </div>
                        </div>
                    </section>
                    <!-- /Experience -->

                    <!-- Experience perso -->
                    <section class="resume-section experience-section mb-5">
                        <h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3"><?= get_field('experience_perso_titre'); ?></h2>
                        <div class="resume-section-content">
                            <div class="resume-timeline position-relative">
                                <?php
                                $args = array(
                                    'post_type' => 'portofolio',
                                    'post_status' => 'publish',
                                    'posts_per_page' => 3,
                                    'orderby' => 'date',
                                    'order' => 'DESC',
                                    'cat' => 'home',
                                );

                                $loop = new WP_Query( $args );

                                while ( $loop->have_posts() ){
                                    $loop->the_post();
                                    ?>
                                    <article class="resume-timeline-item position-relative pb-5">
                                        <div class="resume-timeline-item-header mb-2">
                                            <div class="d-flex flex-column flex-md-row">
                                                <h3 class="resume-position-title font-weight-bold mb-1"><?= the_title(); ?></h3>
                                            </div><!--//row-->
                                            <div class="resume-position-time"><a href="<?php the_field('url'); ?>"><?php the_field('url'); ?></a></div>
                                            <div class="resume-position-time"><?= get_field('periode'); ?></div>
                                        </div>

                                        <div class="resume-timeline-item-desc">
                                            <?php the_field('excerpt_cv'); ?>
                                        </div>
                                    </article>
                                    <?php
                                }
                                wp_reset_postdata();
                                ?>
                            </div>
                        </div>
                    </section>
                    <!-- /Experience perso -->


                </div>
                <div class="col-lg-5">

                    <!-- Skills -->
                    <section class="resume-section skills-section mb-5">
                        <h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3"><?= get_field('competence_titre'); ?></h2>
                        <div class="resume-section-content">

                            <!-- Backend -->
                            <div class="resume-skill-item">
                                <h4 class="resume-skills-cat font-weight-bold"><?= get_field('competence_backend_titre'); ?></h4>
                                <ul class="list-unstyled mb-4">
                                    <?php
                                    $args = array(
                                        'post_type' => 'skill_backend',
                                        'post_status' => 'publish',
                                        'posts_per_page' => -1,
                                        'orderby' => 'date',
                                        'order' => 'DESC',
                                        'cat' => 'home',
                                    );

                                    $loop = new WP_Query( $args );

                                    while ( $loop->have_posts() ){
                                        $loop->the_post();
                                        ?>
                                        <li class="mb-2">
                                            <div class="resume-skill-name"><?= the_title(); ?></div>
                                            <div class="progress resume-progress">
                                                <div class="progress-bar theme-progress-bar-dark" role="progressbar" style="width: <?php the_field('pourcentage'); ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                    wp_reset_postdata();
                                    ?>
                                </ul>
                            </div>

                            <!-- Frontend -->
                             <div class="resume-skill-item">
                                <h4 class="resume-skills-cat font-weight-bold"><?= get_field('competence_frontend_titre'); ?></h4>
                                <ul class="list-unstyled mb-4">
                                    <?php
                                    $args = array(
                                        'post_type' => 'skill_frontend',
                                        'post_status' => 'publish',
                                        'posts_per_page' => -1,
                                        'orderby' => 'date',
                                        'order' => 'DESC',
                                        'cat' => 'home',
                                    );

                                    $loop = new WP_Query( $args );

                                    while ( $loop->have_posts() ){
                                        $loop->the_post();
                                        ?>
                                        <li class="mb-2">
                                            <div class="resume-skill-name"><?= the_title(); ?></div>
                                            <div class="progress resume-progress">
                                                <div class="progress-bar theme-progress-bar-dark" role="progressbar" style="width: <?php the_field('pourcentage'); ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                    wp_reset_postdata();
                                    ?>
                                </ul>
                            </div>

                            <!-- OTher -->
                            <div class="resume-skill-item">
                                <h4 class="resume-skills-cat font-weight-bold"><?= get_field('competence_autre_titre'); ?></h4>
                                <ul class="list-inline">
                                    <?php
                                    $args = array(
                                        'post_type' => 'skill_other',
                                        'post_status' => 'publish',
                                        'posts_per_page' => -1,
                                        'orderby' => 'date',
                                        'order' => 'DESC',
                                        'cat' => 'home',
                                    );

                                    $loop = new WP_Query( $args );

                                    while ( $loop->have_posts() ){
                                        $loop->the_post();
                                        ?>
                                        <li class="list-inline-item"><span class="badge badge-light"><?= the_title(); ?></span></li>
                                        <?php
                                    }
                                    wp_reset_postdata();
                                    ?>
                                </ul>
                            </div>


                        </div>
                    </section>
                    <!-- ./Skills -->

                    <!-- Certification -->
                    <section class="resume-section education-section mb-5">
                        <h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3"><?= get_field('certification_titre'); ?></h2>

                        <div class="resume-section-content">
                            <ul class="list-unstyled resume-awards-list">

                                <?php
                                $args = array(
                                    'post_type' => 'awards',
                                    'post_status' => 'publish',
                                    'posts_per_page' => -1,
                                    'orderby' => 'date',
                                    'order' => 'DESC',
                                    'cat' => 'home',
                                );

                                $loop = new WP_Query( $args );

                                while ( $loop->have_posts() ){
                                    $loop->the_post();
                                    ?>
                                    <li class="mb-2 pl-4 position-relative">
                                        <i class="resume-award-icon fas fa-trophy position-absolute" data-fa-transform="shrink-2"></i>
                                        <div class="resume-award-name"><?= get_field('periode'); ?></div>
                                        <div class="resume-award-desc"><?php the_content(); ?></div>
                                    </li>
                                    <?php
                                }
                                wp_reset_postdata();
                                ?>

                            </ul>
                        </div>
                    </section>
                    <!-- ./Certification -->

                    <!-- Formation -->
                    <section class="resume-section education-section mb-5">
                        <h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3"><?= get_field('formation_titre'); ?></h2>

                        <div class="resume-section-content">
                            <ul class="list-unstyled">

                                <?php
                                $args = array(
                                    'post_type' => 'formation',
                                    'post_status' => 'publish',
                                    'posts_per_page' => -1,
                                    'orderby' => 'date',
                                    'order' => 'DESC',
                                    'cat' => 'home',
                                );

                                $loop = new WP_Query( $args );

                                while ( $loop->have_posts() ){
                                    $loop->the_post();
                                    ?>
                                    <li class="mb-2">
                                        <div class="resume-degree font-weight-bold"><?= the_title(); ?></div>
                                        <div class="resume-degree-org"><?= get_field('ecole'); ?></div>
                                        <div class="resume-degree-time"><?= get_field('periode'); ?></div>
                                    </li>
                                    <?php
                                }
                                wp_reset_postdata();
                                ?>

                            </ul>
                        </div>
                    </section>
                    <!-- ./Formation -->

                    <!-- Langues -->
                    <section class="resume-section language-section mb-5">
                        <h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3"><?= get_field('langue_titre'); ?></h2>

                        <div class="resume-section-content">
                            <ul class="list-unstyled resume-lang-list">

                                <?php
                                $args = array(
                                    'post_type' => 'lang',
                                    'post_status' => 'publish',
                                    'posts_per_page' => -1,
                                    'orderby' => 'date',
                                    'order' => 'DESC',
                                    'cat' => 'home',
                                );

                                $loop = new WP_Query( $args );

                                while ( $loop->have_posts() ){
                                    $loop->the_post();
                                    ?>
                                    <li class="mb-2 pl-4 position-relative">
                                        <li class="mb-2">
                                            <span class="resume-lang-name font-weight-bold"><?php the_title(); ?></span>
                                            <small class="text-muted font-weight-normal">(<?php the_field('niveau'); ?>)</small>
                                        </li>
                                    </li>
                                    <?php
                                }
                                wp_reset_postdata();
                                ?>

                            </ul>
                        </div>
                    </section>
                    <!-- ./Langues -->
                </div>
            </div>





        </div>


    </div>
</article>
